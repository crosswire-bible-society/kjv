# KJV module maintenance

This is for the **King James Version** of the **Holy Bible**, from the [CrossWire Bible Society](https.crosswire.org).

## Description of the **OSIS XML** files:
 * `kjvfull.xml` = the top level **KJV** source file that contains items that are stripped to make `kjv.osis.xml` out before module build
 * `kjv.osis.xml` = the file for building module **KJV**
 * `kjvdc.xml` = the file with only the Deutero-Canonical books (Apocrypha), used towards building module **KJVA**
 * `kjva.osis.xml` = the file made from `kjv.osis.xml` and `kjvdc.xml` using the shell script `kjvfull2kjva.sh` and then used for building module **KJVA**

### Notes:

1. The shell script inserts the bookGroup **div** for the DC books in between the OT & NT bookGroup **div** elements.
2. A line in the shell script has to be modified after any changes in the header!
3. Some comment lines in the script are written in French.
4. The file `kjvdc.xml` was reverse engineered in January 2017 from the existing module **KJVA** version 3.0.1 and subsequently enhanced with various minor corrections and improvements.
5. The OSIS markup in the DC books was then minimal, so very few features (if any) were lost.
6. The file `kjvdc.xml` could also be used to build a **KJVDC** module for testing purposes.
7. CrossWire does not intend to release a separate module called **KJVDC**.
8. Once module **KJVA** version 3.1 is released, this will be its first significant update since January 2014.






## Description of the module **CONF** files:
 * `kjv.conf` = used to define the **KJV** module
 * `kjva.conf` = used to define the **KJVA** module
 * `kjvdc.conf` = could be used to define a **KJVDC** module (e.g. for testing purposes)

NB. _The latter is not necessarily kept fully up to date_!

For further details, including a road map, please refer to https://wiki.crosswire.org/CrossWire_KJV

## Module versions (quick reference)
 * **KJV**		Version=3.1 (released on 26 July 2023)
 * **KJVA**		Version=3.1 (released on 26 July 2023)
 * **KJVDC**	Version=1.2 (_not for release_ )

 ### Notes:
1. The **KJV** and **KJVA** modules now have the same version number, which reflects that they are now  maintained here as one project.
2. This may not always be the case, as any future changes in the DC books will only require the version of the **KJVA** module to be incremented. 
3. A few general changes already implemented for the DC books are still in the queue for the PC books.
